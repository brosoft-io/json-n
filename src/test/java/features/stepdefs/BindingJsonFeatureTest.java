package features.stepdefs;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class BindingJsonFeatureTest {

	@Given("a .json that represents the desired POJO")
	public void a_json_that_represents_the_desired_POJO() {
	    // Write code here that turns the phrase above into concrete actions
	    throw new cucumber.api.PendingException();
	}

	@When("the .json file is read and mapped to the POJO class")
	public void the_json_file_is_read_and_mapped_to_the_POJO_class() {
	    // Write code here that turns the phrase above into concrete actions
	    throw new cucumber.api.PendingException();
	}

	@Then("generate a new instance of the POJO that contains the mapped values from the .json")
	public void generate_a_new_instance_of_the_POJO_that_contains_the_mapped_values_from_the_json() {
	    // Write code here that turns the phrase above into concrete actions
	    throw new cucumber.api.PendingException();
	}

	@Given("a POJO that represents the desired .json file")
	public void a_POJO_that_represents_the_desired_json_file() {
	    // Write code here that turns the phrase above into concrete actions
	    throw new cucumber.api.PendingException();
	}

	@When("the POJO is serialized")
	public void the_POJO_is_serialized() {
	    // Write code here that turns the phrase above into concrete actions
	    throw new cucumber.api.PendingException();
	}

	@Then("generate a new .json file that contains the values mapped from the POJO")
	public void generate_a_new_json_file_that_contains_the_values_mapped_from_the_POJO() {
	    // Write code here that turns the phrase above into concrete actions
	    throw new cucumber.api.PendingException();
	}
}
