package features.beans;

import io.brosoft.jsonn.annotation.JBean;
import io.brosoft.jsonn.annotation.JField;

@JBean
public class HelloWorld {

	@JField
	private String hello;

	public String getHello() {
		return hello;
	}

	public void setHello(String hello) {
		this.hello = hello;
	}
	
}
