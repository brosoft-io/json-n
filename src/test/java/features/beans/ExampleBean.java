package features.beans;

import io.brosoft.jsonn.annotation.JBean;
import io.brosoft.jsonn.annotation.JEnum;
import io.brosoft.jsonn.annotation.JEnumValue;
import io.brosoft.jsonn.annotation.JField;

@JBean
public class ExampleBean {

	@JEnum(representation = JEnumValue.STRING)
	public enum ExampleEnum {
		ENUM_1, ENUM_2, ENUM_3
	}
	
	@JField(key = "character-string")
	private String characterString;
	@JField(key = "integer-number")
	private long integerNumber;
	@JField(key = "floating-number")
	private double floatingNumber;
	@JField(key = "string-array")
	private String[] stringArray;
	@JField(key = "simple-enum")
	private ExampleEnum simpleEnum;
	@JField(key = "sub-object")
	private HelloWorld subObject;
	
	
	public String getCharacterString() {
		return characterString;
	}
	public void setCharacterString(String characterString) {
		this.characterString = characterString;
	}
	public double getFloatingNumber() {
		return floatingNumber;
	}
	public void setFloatingNumber(double floatingNumber) {
		this.floatingNumber = floatingNumber;
	}
	public String[] getStringArray() {
		return stringArray;
	}
	public void setStringArray(String[] stringArray) {
		this.stringArray = stringArray;
	}
	public ExampleEnum getSimpleEnum() {
		return simpleEnum;
	}
	public void setSimpleEnum(ExampleEnum simpleEnum) {
		this.simpleEnum = simpleEnum;
	}
	public HelloWorld getSubObject() {
		return subObject;
	}
	public void setSubObject(HelloWorld subObject) {
		this.subObject = subObject;
	}
	public long getIntegerNumber() {
		return integerNumber;
	}
	public void setIntegerNumber(long integerNumber) {
		this.integerNumber = integerNumber;
	}
}
