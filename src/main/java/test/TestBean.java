package test;

import io.brosoft.jsonn.annotation.JBean;
import io.brosoft.jsonn.annotation.JField;

@JBean
public class TestBean {

	@JField(key="test-variable")
	private String testVariable;
	
	@JField
	private float something;
	
	
	private int hidden;


	public String getTestVariable() {
		return testVariable;
	}


	public void setTestVariable(String testVariable) {
		this.testVariable = testVariable;
	}


	public float getSomething() {
		return something;
	}


	public void setSomething(float something) {
		this.something = something;
	}


	public int getHidden() {
		return hidden;
	}


	public void setHidden(int hidden) {
		this.hidden = hidden;
	}
}
