package test;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

public class Main {

	public static void main(String[] args) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, SecurityException, NoSuchFieldException {
		
		Class<?> clazz = TestBean.class;
		
		System.out.println(TestBean.class.getConstructors()[0]);
		
		
		
		for (Field field : clazz.getDeclaredFields()) {
			System.out.println(field.getName());
		}
		
//		System.out.println(clazz.getDeclaredField("hidden").setAccessible(true));
		
//		instance.getClass().getDeclaredField("hidden").setAccessible(true);
		
		
		Object instance = TestBean.class.getConstructors()[0].newInstance();
		Field field = instance.getClass().getDeclaredField("hidden");
		field.setAccessible(true);
		field.setInt(instance, 56);
		System.out.println("instance int: " + ((TestBean)instance).getHidden());
		
		
 	}

}
