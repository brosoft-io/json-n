package io.brosoft.jsonn.jleaf;

import java.math.BigInteger;

import io.brosoft.jsonn.JLeaf;
import io.brosoft.jsonn.JLeafType;

public class BigIntegerLeaf extends JLeaf<BigInteger> {

	public BigIntegerLeaf(BigInteger value) {
		super(value);
		super.type = JLeafType.BIG_INTEGER;
	}
	
	@Override
	public BigInteger getBigInteger() {
		return super.value;
	}

}
