package io.brosoft.jsonn.jleaf;

import io.brosoft.jsonn.JLeaf;
import io.brosoft.jsonn.JLeafType;

public class NullLeaf extends JLeaf<Object> {

	public NullLeaf(Object value) {
		super(null);
		super.type = JLeafType.NULL;
	}
	
}
