package io.brosoft.jsonn.jleaf;

import io.brosoft.jsonn.JLeaf;
import io.brosoft.jsonn.JLeafType;

public class BytesLeaf extends JLeaf<Byte[]> {

	public BytesLeaf(Byte[] value) {
		super(value);
		super.type = JLeafType.BYTES;
	}
	
	@Override
	public Byte[] getBytes() {
		return super.value;
	}

}
