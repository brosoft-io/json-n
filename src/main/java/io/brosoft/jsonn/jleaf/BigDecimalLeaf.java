package io.brosoft.jsonn.jleaf;

import java.math.BigDecimal;

import io.brosoft.jsonn.JLeaf;
import io.brosoft.jsonn.JLeafType;

public class BigDecimalLeaf extends JLeaf<BigDecimal> {

	public BigDecimalLeaf(BigDecimal value) {
		super(value);
		type = JLeafType.BIG_DECIMAL;
	}
	
	@Override
	public BigDecimal getBigDecimal() {
		return super.value;
	}

}
