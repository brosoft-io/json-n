package io.brosoft.jsonn.jleaf;

import io.brosoft.jsonn.JLeaf;
import io.brosoft.jsonn.JLeafType;

public class StringLeaf extends JLeaf<String> {

	public StringLeaf(String value) {
		super(value);
		type = JLeafType.STRING;
	}
	
	@Override
	public String getString() {
		return super.value;
	}

}
