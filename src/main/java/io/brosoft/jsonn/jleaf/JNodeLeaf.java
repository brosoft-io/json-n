package io.brosoft.jsonn.jleaf;

import io.brosoft.jsonn.JLeaf;
import io.brosoft.jsonn.JLeafType;
import io.brosoft.jsonn.JNode;

public class JNodeLeaf extends JLeaf<JNode> {

	public JNodeLeaf(JNode value) {
		super(value);
		super.type = JLeafType.OBJECT;
	}
	
	@Override
	public JNode getObject() {
		return super.value;
	}

}
