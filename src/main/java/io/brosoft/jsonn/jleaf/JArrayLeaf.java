package io.brosoft.jsonn.jleaf;

import io.brosoft.jsonn.JArray;
import io.brosoft.jsonn.JLeaf;
import io.brosoft.jsonn.JLeafType;

public class JArrayLeaf extends JLeaf<JArray<?>> {

	public <T> JArrayLeaf(JArray<T> value) {
		super(value);
		super.type = JLeafType.ARRAY;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public JArray<?> getJArray() {
		return value;
	}
}
