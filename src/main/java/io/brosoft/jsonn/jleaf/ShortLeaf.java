package io.brosoft.jsonn.jleaf;

import io.brosoft.jsonn.JLeaf;
import io.brosoft.jsonn.JLeafType;

public class ShortLeaf extends JLeaf<Short> {

	public ShortLeaf(Short value) {
		super(value);
		super.type = JLeafType.SHORT;
	}
	
	@Override
	public Short getShort() {
		return super.value;
	}

}
