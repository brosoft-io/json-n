package io.brosoft.jsonn.jleaf;

import io.brosoft.jsonn.JLeaf;
import io.brosoft.jsonn.JLeafType;

public class FloatLeaf extends JLeaf<Float> {

	public FloatLeaf(Float value) {
		super(value);
		super.type = JLeafType.FLOAT;
	}
	
	@Override
	public Float getFloat() {
		return super.value;
	}

}
