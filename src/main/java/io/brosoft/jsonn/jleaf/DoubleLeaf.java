package io.brosoft.jsonn.jleaf;

import io.brosoft.jsonn.JLeaf;
import io.brosoft.jsonn.JLeafType;

public class DoubleLeaf extends JLeaf<Double> {

	public DoubleLeaf(Double value) {
		super(value);
		super.type = JLeafType.DOUBLE;
	}
	
	@Override
	public Double getDouble() {
		return super.value;
	}

}
