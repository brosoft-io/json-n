package io.brosoft.jsonn.jleaf;

import io.brosoft.jsonn.JLeaf;
import io.brosoft.jsonn.JLeafType;

public class IntegerLeaf extends JLeaf<Integer> {

	public IntegerLeaf(Integer value) {
		super(value);
		super.type = JLeafType.INTEGER;
	}
	
	@Override
	public Integer getInteger() {
		return super.value;
	}

}
