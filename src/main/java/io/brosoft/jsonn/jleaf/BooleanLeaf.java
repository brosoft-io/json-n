package io.brosoft.jsonn.jleaf;

import io.brosoft.jsonn.JLeaf;
import io.brosoft.jsonn.JLeafType;

public class BooleanLeaf extends JLeaf<Boolean> {

	public BooleanLeaf(Boolean value) {
		super(value);
		super.type = JLeafType.BOOLEAN;
	}

	@Override
	public Boolean getBoolean() {
		return super.value;
	}
}
