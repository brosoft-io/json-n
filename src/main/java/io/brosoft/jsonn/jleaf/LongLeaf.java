package io.brosoft.jsonn.jleaf;

import io.brosoft.jsonn.JLeaf;
import io.brosoft.jsonn.JLeafType;

public class LongLeaf extends JLeaf<Long> {

	public LongLeaf(Long value) {
		super(value);
		super.type = JLeafType.LONG;
	}

	@Override
	public Long getLong() {
		return super.value;
	}
}
