package io.brosoft.jsonn.exception;

public class JBinderException extends Exception {
	public JBinderException(String message, Exception e) {
		super(message, e);
	} 
}
