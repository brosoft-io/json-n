package io.brosoft.jsonn.parser;

import java.io.IOException;

public interface JReader {

	Character next() throws IOException;
}
