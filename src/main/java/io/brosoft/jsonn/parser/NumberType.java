package io.brosoft.jsonn.parser;

public enum NumberType {
	INTEGER, LONG, DOUBLE, BIG_INTEGER, BIG_DOUBLE, NAN
}