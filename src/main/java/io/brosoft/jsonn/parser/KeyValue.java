package io.brosoft.jsonn.parser;

import io.brosoft.jsonn.JLeafType;

public class KeyValue {

	public String key;
	public Object value;
	public JLeafType type = JLeafType.UNKOWN;
	
}
