package io.brosoft.jsonn.parser;

import java.io.IOException;
import java.io.InputStream;

public class JStreamReader implements JReader {

	private InputStream stream;
	
	public JStreamReader(InputStream stream) {
		this.stream = stream;
	}
	
	@Override
	public Character next() throws IOException {
		if (stream.available() != 0) {
			return (char) stream.read();
		} else {
			new StringBuilder(stream.read());
			return null;
		}
	}

}
