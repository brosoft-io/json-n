package io.brosoft.jsonn.parser;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.BigInteger;

import io.brosoft.jsonn.JArray;
import io.brosoft.jsonn.JLeaf;
import io.brosoft.jsonn.JNode;
import io.brosoft.jsonn.jleaf.BigDecimalLeaf;
import io.brosoft.jsonn.jleaf.BigIntegerLeaf;
import io.brosoft.jsonn.jleaf.BooleanLeaf;
import io.brosoft.jsonn.jleaf.DoubleLeaf;
import io.brosoft.jsonn.jleaf.IntegerLeaf;
import io.brosoft.jsonn.jleaf.JArrayLeaf;
import io.brosoft.jsonn.jleaf.JNodeLeaf;
import io.brosoft.jsonn.jleaf.LongLeaf;
import io.brosoft.jsonn.jleaf.NullLeaf;
import io.brosoft.jsonn.jleaf.StringLeaf;

public abstract class SimpleJParser2 {

	public enum State {
			OBJECT, KEY, VALUE, VALUE_END
	}
	
	public static JNode parse(String jsonString) throws IOException {
		return parse(new JStringReader(jsonString));
	}

	public static JNode parse(InputStream stream) throws IOException {
		return parse(new JStreamReader(stream));
	}
	
	public static JNode parse(JReader reader) throws IOException {
		Character next;
		JNode jNode = new JNode();
		while ((next = reader.next()) != null) {
			switch(next) {
			case '{':
				jNode = parseObject(reader);
				break;
			default :
				break;
			}
		}
		return jNode;
	}
	
	private static JNode parseObject(JReader reader) throws IOException {
		Character next;
		State state = State.KEY;
		JNode jNode = new JNode();
		String key = null;
		while ((next = reader.next()) != null) {
			switch (state) {
			case KEY:
				if (next == '"') {
					key = parseString(reader);
					state = State.VALUE;
				} else if (next == '}') {
					return jNode;
				}
				break;
			case VALUE:
				if (next == ':') {
					state = State.VALUE_END;
					Pair<JLeaf<?>, Character> pair = parseObjectValue(reader);
					jNode.putJLeaf(key, pair.first);
					if (pair.second == ',') {
						state = State.KEY;
					} else if (pair.second=='}') {
						return jNode;
					}
				}
				break;
			case VALUE_END:
				switch (next) {
				case ',':
					state = State.KEY;
					break;
				case '}':
					return jNode;
				}
				break;
			default:
				break;
			}
		}
		// TODO make Invalid JSON exception
		throw new RuntimeException();
	}
	
	private static Pair<JLeaf<?>,Character> parseObjectValue(JReader reader) throws IOException {
		final String OTHER_REGEX = "[nft]";
		final String NUM_REGEX = "[-\\d]";
		String nextString;
		Character next;
		while((nextString = (next = reader.next()).toString()) != null) {
			switch (next) {
			case '"':
				return new Pair<JLeaf<?>, Character>(new StringLeaf(parseString(reader)), '"');
			case '[':
				return new Pair<JLeaf<?>, Character>(new JArrayLeaf(parseArray(reader)), ']');
			case '{':
				return new Pair<JLeaf<?>, Character>(new JNodeLeaf(parseObject(reader)), '}');
			default:
				if (nextString.matches(NUM_REGEX)) {
					Trio<String, NumberType, Character> numValue = parseNumber(reader, next);
					switch (numValue.second) {
					case INTEGER:
						return new Pair<JLeaf<?>, Character>(new IntegerLeaf(Integer.parseInt(numValue.first)), numValue.third);
					case LONG:
						return new Pair<JLeaf<?>, Character>(new LongLeaf(Long.parseLong(numValue.first)), numValue.third);
					case BIG_INTEGER:
						return new Pair<JLeaf<?>, Character>(new BigIntegerLeaf(new BigInteger(numValue.first)), numValue.third);
					case DOUBLE:
						return new Pair<JLeaf<?>, Character>(new DoubleLeaf(Double.parseDouble(numValue.first)), numValue.third);
					case BIG_DOUBLE:
						return new Pair<JLeaf<?>, Character>(new BigDecimalLeaf(new BigDecimal(numValue.first)), numValue.third);
					case NAN:
						// TODO make Invalid JSON exception
						throw new RuntimeException();
					}
				} else if (nextString.matches(OTHER_REGEX)) {
					Boolean otherLeaf = parseOther(reader, next);
					if (otherLeaf == null) {
						return new Pair<JLeaf<?>, Character>(new NullLeaf(otherLeaf), 'l');
					} else {
						return new Pair<JLeaf<?>, Character>(new BooleanLeaf(otherLeaf), 'e');
					}
				}
				break;
			}
		}
		// TODO make Invalid JSON exception
		throw new RuntimeException();
	}
	
	private static JArray<Object> parseArray(JReader reader) throws IOException {
		State arrayState = State.VALUE;
		final String OTHER_REGEX = "[nft]";
		final String NUM_REGEX = "[-\\d]";
		JArray<Object> jArray = new JArray<Object>();
		String nextString;
		Character next;
		while((nextString = (next = reader.next()).toString()) != null) {
			switch (arrayState) {
			case VALUE:
				switch (next) {
				case ']':
					return jArray;
				case '"':
					jArray.add(parseString(reader));
					arrayState = State.VALUE_END;
					break;
				case '[':
					jArray.add(parseArray(reader).asArrayList());
					arrayState = State.VALUE_END;
					break;
				case '{':
					jArray.add(parseObject(reader));
					arrayState = State.VALUE_END;
					break;
				default:
					if (nextString.matches(NUM_REGEX)) {
						Trio<String, NumberType, Character> numValue = parseNumber(reader, next);
						switch (numValue.second) {
						case INTEGER:
							jArray.add(Integer.parseInt(numValue.first));
							break;
						case LONG:
							jArray.add(Long.parseLong(numValue.first));
							break;
						case BIG_INTEGER:
							jArray.add(new BigInteger(numValue.first));
							break;
						case DOUBLE:
							jArray.add(Double.parseDouble(numValue.first));
							break;
						case BIG_DOUBLE:
							jArray.add(new BigDecimal(numValue.first));
							break;
						case NAN:
							// TODO make Invalid JSON exception
							throw new RuntimeException();
						}
						if (numValue.third==',') {
							arrayState = State.VALUE;
						} else if (numValue.third==']') {
							return jArray;
						} else {
							arrayState = State.VALUE_END;
						}
					} else if (nextString.matches(OTHER_REGEX)) {
						jArray.add(parseOther(reader, next));
						arrayState = State.VALUE_END;
					}
					break;
				}
				break;
			case VALUE_END:
				switch (next) {
				case ',':
					arrayState = State.VALUE;
					break;
				case ']':
					return jArray;
				default:
					break;
				}
				break;
			default:
				break;
			}
		}
		// TODO make Invalid JSON exception
		throw new RuntimeException();
	}
	
	private static Boolean parseOther(JReader reader, Character first) throws IOException {
		final String nullString = "null";
		final String trueString = "true";
		final String falseString = "false";
		StringBuilder builder = new StringBuilder().append(first);
		Character next;
		String currentString;
		while ((next = reader.next()) != null && (currentString = builder.append(next).toString()).length() < 6) {
			switch (currentString) {
			case nullString:
				return null;
			case trueString:
				return true;
			case falseString:
				return false;
			}
		}
		// TODO make Invalid JSON exception
		throw new RuntimeException();
	}
	
	private static Trio<String,NumberType,Character> parseNumber(JReader reader, Character first) throws IOException {
		StringBuilder builder = new StringBuilder().append(first);
		Character next;
		String number = null;
		final String NUM_REGEX = "[\\d\\-\\.eE\\+]";
		final String INT_REGEX = "^[\\-]?[\\d]{1,9}$";
		final String LONG_REGEX = "^[\\-]?[\\d]{10,18}$";
		final String BIG_INT_REGEX = "^[\\-]?[\\d]{19,}$";
		final String DOUBLE_REGEX = "^[\\-]?[\\d]+\\.\\d+([eE][\\-\\+]?\\d{1,2})?$";
		final String BIG_DECI_REGEX = "^[\\-]?[\\d]+\\.\\d+([eE][\\-\\+]?\\d{3,})?$";
		while ((next = reader.next()) != null) {
			if (next.toString().matches(NUM_REGEX)) {
				builder.append(next);
			} else {
				number = builder.toString();
				NumberType type;
				if (number.matches(INT_REGEX)) {
					type = NumberType.INTEGER;
				} else if (number.matches(LONG_REGEX)) {
					type = NumberType.LONG;
				} else if (number.matches(DOUBLE_REGEX)) {
					type = NumberType.DOUBLE;
				} else if (number.matches(BIG_INT_REGEX)) {
					type = NumberType.BIG_INTEGER;
				} else if (number.matches(BIG_DECI_REGEX)) {
					type = NumberType.BIG_DOUBLE;
				} else {
					type = NumberType.NAN;
				}
				return new Trio<String, NumberType, Character>(number,type,next);
			}
		}
		// TODO make Invalid JSON exception
		throw new RuntimeException();
	}
	
	private static String parseString(JReader reader) throws IOException {
		Character next;
		StringBuilder builder = new StringBuilder();
		boolean escape = false;
		while ((next = reader.next()) != null) {
			if (!escape) {
				switch (next) {
				case '"':
					return builder.toString();
				default:
					builder.append(next);
				}
			} else {
				builder.append(next);
				escape = false;
			}
		}
		throw new RuntimeException();
	}
}
