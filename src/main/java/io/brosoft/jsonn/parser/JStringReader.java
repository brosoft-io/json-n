package io.brosoft.jsonn.parser;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

public class JStringReader implements JReader {

	private int currentIndex;
	private int length;
	private char[] array;
	
	public JStringReader(String string) {
		array = string.toCharArray();
		this.currentIndex = 0;
		this.length = this.array.length;
	}
	
	public JStringReader(char[] array) {
		this.array = array;
		this.currentIndex = 0;
		this.length = this.array.length;
	}
	
	public JStringReader(byte[] array) {
		this.array = Charset.forName("UTF-8").decode(ByteBuffer.wrap(array)).array();
		this.currentIndex = 0;
		this.length = this.array.length;
	}
	
	@Override
	public Character next() {
		if (currentIndex < length) {
			currentIndex++;
//			System.out.println(array[currentIndex - 1]);
			return array[currentIndex - 1];
		} else {
			return null;
		}
	}

}
