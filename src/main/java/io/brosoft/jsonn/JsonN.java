package io.brosoft.jsonn;

import java.io.InputStream;
import java.io.OutputStream;

public abstract class JsonN {
	
	public static JNode parse(String jsonString) {
		return null;
	}
	
	public static JNode parse(InputStream jsonStream) {
		return null;
	}
	
	public static <T> T map(String jsonString, Class<?> clazz) {
		return null;
	}
	
	public static <T> T map(InputStream jsonStream, Class<?> clazz) {
		return null;
	}
	
	public static <T> String stringSerialize(T t) {
		return null;
	}
	
	public static <T> String stringSerialize(T t, int indent) {
		return null;
	}
	
	public static <T> OutputStream streamSerialize(T t) {
		return null;
	}
}
