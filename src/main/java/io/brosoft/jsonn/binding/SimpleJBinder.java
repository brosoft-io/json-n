package io.brosoft.jsonn.binding;

import java.lang.reflect.InvocationTargetException;

import io.brosoft.jsonn.JNode;
import io.brosoft.jsonn.exception.JBinderException;

public class SimpleJBinder {

	private Class<?> clazz;
	private JNode jNode;
	private Object instance;
	
	public SimpleJBinder(Class<?> clazz, JNode jNode) {
		this.clazz = clazz;
		this.jNode = jNode;
	}
	
	public Object bind() throws JBinderException {
		try {
			instance = clazz.getConstructors()[0].newInstance();
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| SecurityException e) {
			e.printStackTrace();
			throw new JBinderException("",e);
		}
		return instance;
	}
}
