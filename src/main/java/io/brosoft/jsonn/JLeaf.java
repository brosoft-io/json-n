package io.brosoft.jsonn;

import java.math.BigDecimal;
import java.math.BigInteger;

public abstract class JLeaf<T> {
	
	protected JLeafType type;
	protected T value;
	
	public JLeaf(T value) {
		this.type = JLeafType.UNKOWN;
		this.value = value;
	}
	
	public JLeafType getType() {
		return type;
	}

	public T getValue() {
		return this.value;
	}
	
	public JNode getObject() {
		return null;
	}
	
	public String getString() {
		return null;
	}
	
	public Integer getInteger() {
		return null;
	}
	
	public Double getDouble() {
		return null;
	}
	
	public Long getLong() {
		return null;
	}
	
	public Short getShort() {
		return null;
	}

	public Float getFloat() {
		return null;
	}
	
	public Boolean getBoolean() {
		return null;
	}
	
	public Byte[] getBytes() {
		return null;
	}
	
	public BigInteger getBigInteger() {
		return null;
	}
	
	public BigDecimal getBigDecimal() {
		return null;
	}

	public <A> JArray<A> getJArray() {
		return null;
	}
	
}
