package io.brosoft.jsonn;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import io.brosoft.jsonn.jleaf.BigDecimalLeaf;
import io.brosoft.jsonn.jleaf.BigIntegerLeaf;
import io.brosoft.jsonn.jleaf.BooleanLeaf;
import io.brosoft.jsonn.jleaf.BytesLeaf;
import io.brosoft.jsonn.jleaf.DoubleLeaf;
import io.brosoft.jsonn.jleaf.FloatLeaf;
import io.brosoft.jsonn.jleaf.IntegerLeaf;
import io.brosoft.jsonn.jleaf.JArrayLeaf;
import io.brosoft.jsonn.jleaf.JNodeLeaf;
import io.brosoft.jsonn.jleaf.LongLeaf;
import io.brosoft.jsonn.jleaf.NullLeaf;
import io.brosoft.jsonn.jleaf.ShortLeaf;
import io.brosoft.jsonn.jleaf.StringLeaf;

public class JNode {
	
	protected Map<String,JLeaf<?>> map; 
	
	public JNode() {
		map = new HashMap<String, JLeaf<?>>();
	}
	
	public boolean isNode(String key) {
		return map.get(key) != null;
	}
	
	public JLeafType getType(String key) {
		return map.get(key).getType();
	}
	
	public Set<String> getKeys() {
		return map.keySet();
	}
	
	public JNode putJLeaf(String key, JLeaf<?> leaf) {
		map.put(key, leaf);
		return this;
	}
	
	public JNode putObject(String key, JNode value) {
		map.put(key, new JNodeLeaf(value));
		return this;
	}
	
	public JNode getObject(String key) {
		return map.get(key).getObject();
	}
	
	public <T> JNode putArray(String key, JArray<T> list) {
		map.put(key, new JArrayLeaf(list));
		return this;
	}
	
	public <T> JNode putArray(String key, List<T> list) {
		map.put(key, new JArrayLeaf(new JArray<T>(list)));
		return this;
	}
	
	public <T> JNode putArray(String key, @SuppressWarnings("unchecked") T... list) {
		map.put(key, new JArrayLeaf(new JArray<T>(list)));
		return this;
	}
	
	public <T> JArray<T> getArray(String key) {
		return map.get(key).getJArray();
	}
	
	public JNode putString(String key, String value) {
		map.put(key, new StringLeaf(value));
		return this;
	}
	
	public String getString(String key) {
		return map.get(key).getString(); 
	}
	
	public JNode putInteger(String key, Integer value) {
		map.put(key, new IntegerLeaf(value));
		return this;
	}
	
	public Integer getInteger(String key) {
		return map.get(key).getInteger();
	}
	
	public JNode putDouble(String key, Double value) {
		map.put(key, new DoubleLeaf(value));
		return this;
	}
	
	public Double getDouble(String key) {
		return map.get(key).getDouble();
	}
	
	public JNode putShort(String key, Short value) {
		map.put(key, new ShortLeaf(value));
		return this;
	}
	
	public Short getShort(String key) {
		return map.get(key).getShort();
	}
	
	public JNode putLong(String key, Long value) {
		map.put(key, new LongLeaf(value));
		return this;
	}
	
	public Long getLong(String key) {
		return map.get(key).getLong();
	}
	
	public JNode putFloat(String key, Float value) {
		map.put(key, new FloatLeaf(value));
		return this;
	}
	
	public Float getFloat(String key) {
		return map.get(key).getFloat();
	}
	
	public JNode putBoolean(String key, Boolean value) {
		map.put(key, new BooleanLeaf(value));
		return this;
	}
	
	public Boolean getBoolean(String key) {
		return map.get(key).getBoolean();
	}
	
	public JNode putNull(String key) {
		map.put(key, new NullLeaf(null));
		return this;
	}
	
	public Boolean isNull(String key) {
		return map.get(key).getType() == JLeafType.NULL;
	}
	
	public Boolean exists(String key) {
		return map.containsKey(key);
	}
	
	public JNode putBigInteger(String key, BigInteger value) {
		map.put(key, new BigIntegerLeaf(value));
		return this;
	}
	
	public BigInteger getBigInteger(String key) {
		return map.get(key).getBigInteger();
	}
	
	public JNode putBigDecimal(String key, BigDecimal value) {
		map.put(key, new BigDecimalLeaf(value));
		return this;
	}
	
	public BigDecimal getBigDecimal(String key) {
		return map.get(key).getBigDecimal();
	}
	
	public JNode putBytes(String key, Byte[] value) {
		map.put(key, new BytesLeaf(value));
		return this;
	}
	
	public Byte[] getBytes(String key) {
		return map.get(key).getBytes();
	}
}
