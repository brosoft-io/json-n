package io.brosoft.jsonn;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class JArray<T> {

	private List<T> list;
	
	public JArray() {
		list = new ArrayList<T>();
	}
	
	@SafeVarargs
	public JArray(T... t) {
		list = new ArrayList<T>(Arrays.asList(t));
	}
	
	public JArray(Collection<T> list) {
		this.list = new ArrayList<T>(list);
	}
	
	public JArray<T> add(T t) {
		list.add(t);
		return this;
	}
	
	public T get(int index) {
		return list.get(index);
	}
	
	public JArray<T> remove(int index) {
		list.remove(index);
		return this;
	}
	
	public Boolean contains(T o) {
		return list.contains(o);
	}
	
	public int size() {
		return list.size();
	}
	
	public Boolean isEmpty() {
		return list.isEmpty();
	}
	
	public List<T> asArrayList() {
		return list;
	}
	
	public Iterator<T> asIterator() {
		return list.iterator();
	}
}
