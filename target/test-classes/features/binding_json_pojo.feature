Feature: Binding JSON

    binding json to a POJO to be interchangebly serialized and deserialized

    Scenario: Raw JSON to POJO
        Given a .json that represents the desired POJO
        When the .json file is read and mapped to the POJO class
        Then generate a new instance of the POJO that contains the mapped values from the .json

    Scenario: POJO to raw JSON
        Given a POJO that represents the desired .json file
        When the POJO is serialized
        Then generate a new .json file that contains the values mapped from the POJO